// meetinghub Firmware v1
// made by basti.debug 
// Dec 2023


#pragma region Dependencies 

#include <Wire.h> // IIC 
// Servo
#include <ESP32Servo.h>
// Screen
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>
// Initalisation of the Screen with the Adafruit library

#pragma endregion Dependencies

// Screen Dimentions
#define screen_width 128
#define screen_height 64
#define oled_reset -1
Adafruit_SH1106G display = Adafruit_SH1106G(screen_width, screen_height, &Wire, oled_reset);


// Screen Address
#define screen_address 0x3c

#define fan1pin 16
#define fan2pin 13

#define tachopin1 18
#define tachopin2 19

#define servopin 4

#define hbridgepin1 26
#define hbridgepin2 27

// I2C Slave Address
#define slaveaddress 9


// Variables for rpm measurment
volatile unsigned long fanPulseCount1 = 0;
unsigned long lastmillis1 = 0;

volatile unsigned long fanPulseCount2 = 0;
unsigned long lastmillis2 = 0;


Servo lightservo;


/* Instruction set over I2C
Databyte:    Function :
1            Fan 1 Startup
2            Fan 2 Startup
3            Fan 1 Shutdown
4            Fan 2 Shutdown
5            Table up
6            Table down
7            
*/

#pragma region functions

void FANstartup(int fanpin){
  for (int i = 0; i <= 255; i++) {
    analogWrite(fanpin, i);  // Set fan speed
    delay(10);  // Wait for a short time for the fan to reach the desired speed
  }
}

void FANshutdown(int fanpin){
  for (int i = 255; i >= 0; i--) {
    analogWrite(fanpin, i);  // Set fan speed
    delay(10);  // Wait for a short time for the fan to reach the desired speed
  }
}


void fanTachInterrupt1(){
  fanPulseCount1++;
}
void fanTachInterrupt2(){
  fanPulseCount2++;
}


unsigned long FANrpm1(){
  if (millis()-lastmillis1 >= 1000){
    detachInterrupt(digitalPinToInterrupt(tachopin1));
    unsigned long fanspeed = (fanPulseCount1 * 60) / 2;
    fanPulseCount1 = 0;
    lastmillis1 = millis();
    attachInterrupt(digitalPinToInterrupt(tachopin1), fanTachInterrupt1, RISING); // Reenable interrupt
    return fanspeed;
  }
}

unsigned long FANrpm2(){
  if (millis() - lastmillis2 >= 1000){
    detachInterrupt(digitalPinToInterrupt(tachopin2));
    unsigned long fanspeed = (fanPulseCount2 * 60) / 2;
    fanPulseCount2 = 0;
    lastmillis2 = millis();
    attachInterrupt(digitalPinToInterrupt(tachopin2), fanTachInterrupt2, RISING); // Reenable interrupt
    Serial.println(fanspeed);
  }
}

void TABLEup(){
  digitalWrite(hbridgepin1,HIGH);
  digitalWrite(hbridgepin2,LOW);
}
void TABLEdown(){
  digitalWrite(hbridgepin1,LOW);
  digitalWrite(hbridgepin2,HIGH);
}
void TABLEstop(){
  digitalWrite(hbridgepin1,LOW);
  digitalWrite(hbridgepin2,LOW);
}

#pragma endregion functions

void DISPLAYstartup(){
  display.begin(screen_address, true);
  display.display();

}



void setup() {
  //lightservo.attach(servopin); // attach servo 
  pinMode(fan1pin,OUTPUT); 
  pinMode(fan2pin,OUTPUT);

  // The tachopins are needed for the rpm reading of the fans
  pinMode(tachopin1, INPUT_PULLUP);
  pinMode(tachopin2, INPUT_PULLUP);

  // H bridge pins
  pinMode(hbridgepin1,OUTPUT);
  pinMode(hbridgepin2,OUTPUT);
  // Interrupts needed for the RPM Reading, triggering on rising edge
  attachInterrupt(digitalPinToInterrupt(tachopin1), fanTachInterrupt1, RISING);
  attachInterrupt(digitalPinToInterrupt(tachopin2), fanTachInterrupt2, RISING);

  Serial.begin(115200);
  Serial.println("meetinghub subcontroller online");

  // Start I2c
  Wire.begin(slaveaddress); 
  Wire.onReceive(I2CreceiveData);
  Serial.println("i2c online");

  //DISPLAYstartup();
}

void loop() {

  //display.clearDisplay();
  //display.setTextSize(1);
  //display.setTextColor(SH110X_WHITE);
  
  //.println("meetinghub");
  //display.println("v1");
  
  //lightservo.write(0);

  //display.display();
  TABLEup();
  delay(5000);
  TABLEstop();
  delay(5000);
}

// Handling I2C 
void I2CreceiveData(int byteCount)  {
   while (Wire.available()) {
    int incommingdata = Wire.read();

    if(incommingdata == 1){
      FANstartup(fan1pin);
    }
    if(incommingdata == 2){
      FANstartup(fan2pin);
    }
    if(incommingdata == 3){
      FANshutdown(fan1pin);
    }
    if(incommingdata == 4){
      FANshutdown(fan2pin);
    }
    if(incommingdata == 5){
      
    }
    if(incommingdata == 6){
      
    }
    if(incommingdata == 7){
      
    }
  }
}
