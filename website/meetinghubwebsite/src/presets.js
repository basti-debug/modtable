import React from "react";
import { useEffect, useState } from "react";

import { ThemeProvider, useTheme } from "next-themes";
import {
  User,
  Button,
  NextUIProvider,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Divider,
  Link,
  Image,
  Tabs,
  Tab,
  Slider,
} from "@nextui-org/react";

import "./presets.css";
import { tab } from "@testing-library/user-event/dist/tab";

const colors = ["secondary"];
const deskip = "http://127.0.0.1:8000/";


const PresetComponent = () => {

  const [getselectedTab, setSelectedTab] = useState('meeting');
  const [getbrightness, setBrightness] = useState(0);
  const [getlampPosition, setLampPosition] = useState(0);
  const [getdeskHeight, setDeskHeight] = useState(0);
  const [getfanSpeed, setFanSpeed] = useState(100);



  const sendConfiguration = () => {
    const brightness = getbrightness;
    const lampPosition = getlampPosition;
    const deskHeight = getdeskHeight;
    const fanSpeed = getfanSpeed;
    const selectedTab = getselectedTab;

    const configuration = {
      brightness,
      lampPosition,
      deskHeight,
      fanSpeed,
      selectedTab,
    };

    fetch(deskip + 'configurehotkeys', {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(configuration),
    })
      .then((response) => {
        if (response.ok) {
          console.log("Configuration sent successfully");
        } else {
          console.error("Failed to send configuration");
        }
      })
      .catch((error) => {
        console.error("Error occurred while sending configuration:", error);
      });
  };



  return (
    <NextUIProvider>
      <ThemeProvider attribute="class" defaultTheme="dark">
        <div className="header">
          <style>
            @import
            url('https://fonts.googleapis.com/css2?family=DM+Sans&display=swap');
          </style>
          <User
            className="userbadge"
            name="basti.debug"
            description="Engineer"
          />
          <h2>meetinghub Controller</h2>
          <h1>preset page</h1>
        </div>
        <div className="controls">
          <div className="mselectordiv">
            <label className="selectionlabel">select mode to change</label>
            <div className="modeselector">
              {colors.map((color) => (
                <Tabs
                  key={color}
                  color={color}
                  aria-label="Tabs colors"
                  radius="full"
                  onSelectionChange={(newKey) => setSelectedTab(newKey)}
                >
                  <Tab key="meeting" title="meeting"/>
                  <Tab key="recording" title="recording" />
                  <Tab key="working" title="working"/>
                </Tabs>
              ))}
            </div>
          </div>
          <label className="l1">Light settings</label>
          <Slider label="brightness" className="brightnessslider" onChange={setBrightness}/>
          <Slider label="lamp position" className="lampslider" onChange={setBrightness}/>
          <hr />
          <label className="l1">Desk settings</label>
          <Slider
            className="deskheightslider"
            label="desk height"
            showSteps={true}
            step={10}
            defaultValue={70}
            onChange={setDeskHeight}
            minValue={70}
            maxValue={120}
          />
          <Slider
            className="fanspeedslider"
            label="fan speed"
            showSteps={true}
            minValue={0}
            maxValue={100}
            defaultValue={20}
            onChange={setFanSpeed}
            step={10}
          />
          <Button
            color="success"
            className="saveconfig"
            onClick={sendConfiguration}
          >
            Save Configuration
          </Button>
        </div>
      </ThemeProvider>
    </NextUIProvider>
  );
};

export default PresetComponent;
