import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

// material 3 dependencies
import "@material/web/button/filled-button.js";
import "@material/web/button/outlined-button.js";
import "@material/web/checkbox/checkbox.js";

import SettingsComponent from "./settings.js";
import CalendarComponent from "./calendar.js";
import PresetComponent from "./presets.js";
import Layout from "./navbar.js";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<App />} />
          <Route path="presets" element={<PresetComponent />} />
          <Route path="settings" element={<SettingsComponent />} />
          <Route path="calendar" element={<CalendarComponent />} />
        </Route>
      </Routes>
    </Router>
  </React.StrictMode>
);
