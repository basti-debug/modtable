"use client";

import "./App.css";

import React, { useState, useEffect } from "react";

import { Tabs, Tab, Badge } from "@nextui-org/react";
import { ThemeProvider, useTheme } from "next-themes";
import {
  User,
  Button,
  NextUIProvider,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Divider,
  Link,
  Image,
} from "@nextui-org/react";

function App() {
  const sizes = ["sm", "md", "lg"];
  const colors = ["secondary"];

  const deskip = "http://127.0.0.1:8000/";

  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  if (!mounted) return null;

  const setMode = (key) => {
    
    fetch(deskip + 'hotkey?key='+key, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        if (response.ok) {
          console.log("Configuration sent successfully");	
        } else {
          console.error("Failed to send configuration");
        }
      })
      .catch((error) => {
        console.error("Error occurred while sending configuration:", error);
      });
  };

  

  


  return (
    <NextUIProvider>
      <ThemeProvider attribute="class" defaultTheme="dark">
        <div className="App">
          <div className="header">
            <style>
              @import
              url('https://fonts.googleapis.com/css2?family=DM+Sans&display=swap');
            </style>
            <User
              className="userbadge"
              name="basti.debug"
              description="Developer"
            />
            <h2>meetinghub Controller</h2>
            <h1>home page</h1>
          </div>
          <div className="controls">
            <div className="topselector">
              <div className="mselectordiv">
                <label className="selectionlabel">select mode</label>
                <div className="modeselector">
                  {colors.map((color) => (
                    <Tabs
                      key={color}
                      color={color}
                      aria-label="Tabs colors"
                      radius="full"
                      onSelectionChange={(newKey) => setMode(newKey)}
                    >
                      <Tab key="1" title="meeting" />
                      <Tab key="2" title="recording" />
                      <Tab key="3" title="working" />
                      <Tab key="4" title="manual" />
                    </Tabs>
                  ))}
                </div>
              </div>
            </div>
            <Card className="notification">
              <CardHeader className="flex gap-3">
                <div className="flex flex-col">
                  <p className="text-xl">No new messages</p>
                </div>
              </CardHeader>
              <Divider />
              <CardBody>
                <p>
                  No new notifications. You're all caught up! Check back later
                  for new messages.
                </p>
              </CardBody>
              <Divider />
            </Card>
          </div>
        </div>
      </ThemeProvider>
    </NextUIProvider>
  );
}

export default App;
