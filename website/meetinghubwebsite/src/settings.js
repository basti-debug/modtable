import React, { useEffect, useState } from "react";
import { ThemeProvider, useTheme } from "next-themes";
import { User, NextUIProvider, Switch, Input, Button } from "@nextui-org/react";


import "./settings.css";

const deskip = "http://127.0.0.1:8000/";


const SettingsComponent = () => {

//--------------------State for the Desk and Atem Status---------------------

  const [deskonlineColor, setDeskOnlineColor] = useState("red");
  const [atemonlineColor, setAtemOnlineColor] = useState("red");

  const [labelDesk, setLabelDesk] = useState("Offline");
  const [labelAtem, setLabelAtem] = useState("Offline");

  const [isAtemScanButtonEnabled, setAtemScanButtonDisabled] = useState(true);


  const [autoFanModeEnabled, setAutoFanModeEnabled] = useState(false);

 
 //--------------------Functions---------------------

//--------------------Connecting to the desk---------------------
  const handleRaspIPScanning = async () => {
    const response = await fetch(deskip + 'unitconnected', {
      method: 'DELETE', 
      headers: {
        'Content-Type': 'application/json',
      },
    });
  
    if (!response.ok) {
      const message = `An error has occured: ${response.status}`;
      throw new Error(message);
    }
    if (response.ok) {
      setAtemScanButtonDisabled(false);
      setLabelDesk("Connected");
      setDeskOnlineColor("green");
      
    }
    return response;
  }


  //--------------------Connect to ATEM---------------------
  const handleAtemIPScanning = async () => {
    const response = await fetch(deskip + 'atemconnected', {
      method: 'DELETE', 
      headers: {
        'Content-Type': 'application/json',
      },
    });
  
    if (!response.ok) {
      const message = `An error has occured: ${response.status}`;
      throw new Error(message);
    }
    if (response.ok) {
      setLabelAtem("Conencted");
      setAtemOnlineColor("green");
    }
    return response;
  }

  

//--------------------Automatic fan mode---------------------
const handleAutoFanMode = async (enabled) => {
  const response = await fetch(deskip + 'fanautomode?on='+enabled, {
    method: 'PUT', 
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (!response.ok) {
    const message = `An error has occured: ${response.status}`;
    throw new Error(message);
  }
  if (response.ok) {
    console.log("Auto Fan Mode is now: " + enabled);
  }
  return response;
}


const handleAutoFanModeOn = async () => {
  await handleAutoFanMode(true);
  setAutoFanModeEnabled(true);
};

const handleAutoFanModeOff = async () => {
  await handleAutoFanMode(false);
  setAutoFanModeEnabled(false);
};







  //--------------------Mainline Page---------------------

  return (
    <NextUIProvider>
      <ThemeProvider attribute="class" defaultTheme="dark">
        <div className="header">
          <style>
            @import
            url('https://fonts.googleapis.com/css2?family=DM+Sans&display=swap');
          </style>
          <User
            className="userbadge"
            name="basti.debug"
            description="Engineer"
          />
          <h2>meetinghub Controller</h2>
          <h1>settings page</h1>
        </div>
        <div className="settings">
          <Switch className="darkmodetoggle">Dark mode</Switch>
          <br />
          <Switch
            className="autofantoggle"
            checked={autoFanModeEnabled}
            onChange={autoFanModeEnabled ? handleAutoFanModeOff : handleAutoFanModeOn}
          >
            Auto Fan mode
          </Switch>
          <br />
          <hr />
          <label className="l1">Connection to ATEM</label>
          <br />
          <div className="statussection">
            <div className="deskonline" style={{backgroundColor:atemonlineColor}}/>
            <label className="statuslableDesk">{labelAtem}</label>
          </div>
          <Switch
            className="autoscantoggleATEM"
          >
            Scanning for ATEM IP
          </Switch>
          <br />
          <Input
            isDisabled
            type="ip"
            label="Atem IP Address"
            defaultValue="192.168.212.216"
            className="atemip"
          />
          <Button isDisabled className="connecttoATEM" color="primary">
            Connect
          </Button>
        </div>
        <hr />
        <label className="l1">Connection to Desk</label>
        <br />
        <div className="statussection">
          <div className="deskonline" style={{backgroundColor:deskonlineColor}} />
          <label className="statuslableDesk">{labelDesk}</label>
        </div>
        <Switch
          className="autoscantoggleRASP"
        >
          Scanning for Table IP
        </Switch>
        <br />
        <Input
          type="ip"
          label="Raspberry IP Address"
          defaultValue={deskip}
          className="tableip"
          isDisabled={!isAtemScanButtonEnabled}
        />
        <Button className="connecttoRASP" color="primary" onClick={handleRaspIPScanning} isDisabled={!isAtemScanButtonEnabled}>
          Connect
        </Button>
      </ThemeProvider>
    </NextUIProvider>
  );
};

export default SettingsComponent;