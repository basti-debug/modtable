"use client";

import { Outlet, Link } from "react-router-dom";
import { useLocation } from "react-router-dom";

import { Tabs, Tab } from "@nextui-org/react";
import React, { useState } from "react";

import "./navbar.css";

const colors = ["primary"];

const Layout = () => {
  let location = useLocation();
  const pathname = location.pathname;
  return (
    <>
      <div className="navbar">
        {colors.map((color) => (
          <Tabs
            key={color}
            color={color}
            aria-label="Tabs colors"
            radius="full"
            selectedKey={pathname}
          >
            <Tab key="/" title="home" href="/" />
            <Tab key="/calendar" title="calendar" href="/calendar" />
            <Tab key="/presets" title="presets" href="/presets" />
            <Tab key="/settings" title="settings" href="/settings" />
          </Tabs>
        ))}
      </div>
      <Outlet />
    </>
  );
};

export default Layout;
