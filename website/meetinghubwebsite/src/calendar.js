import React from "react";
import { ThemeProvider, useTheme } from "next-themes";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { DateCalendar } from "@mui/x-date-pickers/DateCalendar";
import {
  User,
  Button,
  NextUIProvider,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  Divider,
  Link,
  Image,
} from "@nextui-org/react";
import { LocalizationProvider } from "@mui/x-date-pickers";

import "./calendar.css";

const CalendarComponent = () => {
  return (
    <NextUIProvider>
      <ThemeProvider attribute="class" defaultTheme="dark">
        <div className="header">
          <style>
            @import
            url('https://fonts.googleapis.com/css2?family=DM+Sans&display=swap');
          </style>
          <User
            className="userbadge"
            name="basti.debug"
            description="Engineer"
          />
          <h2>meetinghub Controller</h2>
          <h1>calendar page</h1>
        </div>
        <LocalizationProvider dateAdapter={AdapterDayjs}>
          <DateCalendar className="calendar" />
        </LocalizationProvider>
      </ThemeProvider>
    </NextUIProvider>
  );
};

export default CalendarComponent;
