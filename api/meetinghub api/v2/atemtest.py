###############################################################################
# meetinghub API v4
# by basti-debug
# created 18.2.2024

# v2 basic up and down functions

# v3 27.2.24 added height related driving functions, 
#    get status calls for the atem

# v4 27.2.24 added basic camera on off call 
#    button integration

# v5 13.3.24 added lighting function basic and api outside access
#    14.3.24 added hotkey changing function 
#            added functionality for api hotkeycalling

###############################################################################




# all imports 
###############################################################################

from fastapi import FastAPI, Body
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

import PyATEMMax
import time
import argparse

# needed for oled screen
from luma.core.interface.serial import i2c
from luma.oled.device import sh1106
from luma.core.render import canvas


# for getting ip address
import socket

#for gpio 
import RPi.GPIO as GPIO

#needed for custom variables
from enum import Enum

# all declarations 
###############################################################################

# API Part
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# struct for recieving configuration jsons
class Configuration(BaseModel):
    brightness: int
    lampPosition: int
    deskHeight: int
    fanSpeed: int
    selectedTab: str


hotkey1Configuration = Configuration(brightness=0, lampPosition=0, deskHeight=0, fanSpeed=0, selectedTab="")
hotkey2Configuration = Configuration(brightness=0, lampPosition=0, deskHeight=0, fanSpeed=0, selectedTab="")
hotkey3Configuration = Configuration(brightness=0, lampPosition=0, deskHeight=0, fanSpeed=0, selectedTab="")

# Atem Part
switcher = PyATEMMax.ATEMMax()

# Oled init
serial = i2c(port=1,address=0x3C)
device = sh1106(serial)

# gpios
fanzone1Pin = 33
fanzone2Pin = 35

button1Pin = 15
button2Pin = 13
button3Pin = 11

motorDirPin = 22
motorPWMPin = 32

gpio1Pin = 16  # button 4
gpio2Pin = 36  # Light servo PWM
gpio3Pin = 29  # Relay 1
gpio4Pin = 31  # Relay 2
gpio5Pin = 37  
gpio6Pin = 18  # Light on 

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

GPIO.setup(fanzone1Pin,GPIO.OUT)
GPIO.setup(fanzone2Pin,GPIO.OUT)

GPIO.setup(button1Pin,GPIO.IN)
GPIO.setup(button2Pin,GPIO.IN)
GPIO.setup(button3Pin,GPIO.IN)

GPIO.setup(motorDirPin,GPIO.OUT)
GPIO.setup(motorPWMPin,GPIO.OUT)

GPIO.setup(gpio1Pin,GPIO.OUT)
GPIO.setup(gpio2Pin,GPIO.OUT)
GPIO.setup(gpio3Pin,GPIO.OUT) # Relay 1
GPIO.setup(gpio4Pin,GPIO.OUT) # Relay 2 
GPIO.setup(gpio5Pin,GPIO.OUT)
GPIO.setup(gpio6Pin,GPIO.OUT)


# intiate the motor pwm
motorPWM = GPIO.PWM(motorPWMPin,100)
motorPWM.start(0)

# invert relay pins 
GPIO.output(gpio3Pin,GPIO.HIGH)
GPIO.output(gpio4Pin,GPIO.HIGH)

device.clear()


###############################################################################

# running variables

###############################################################################

# atem ip 
ipknown = True  #for v5, when true no ip scanning for the atem is done
count = 0
atemip = "192.168.212.93"

debuggingmode = True

class tableMode(Enum):
    work = 1
    meeting = 2 
    off = 3

tableHeight = 70 # current height variable
tableUpSpeedPerSec = 2.8 # how many cm the table rises during 1 second 
talbeDownSpeedPerSec = 3 # how many cm the table lowers during 1 second

tableStartHeight = 70 # lowest height 

atemPreviewChannel = 1 # default Preview Channel on the ATEM controler
atemProgramChannel = 1 # default Program Channel on the ATEM controler
atemBlack = False # default Fade to Black on the ATEM controler 



###############################################################################

# functions

###############################################################################

def notify(text, x, y):
    with canvas(device) as draw:
        draw.text((x,y),text,fill="white")

def notify2(text):
    with canvas(device) as draw:
        draw.rectangle([(0, 40), (250, 100)], fill="white")
        draw.text((5,45),text, fill="black")


def alert(text):
    with canvas(device) as draw:
        draw.rectangle([(0, 40), (250, 100)], fill="white")
        draw.text((5,45),text, fill="black")

def bigstatus(text):
    with canvas(device) as draw:
        draw.text((60,30),text,fill="white")

def getRaspIP():
    # Create a socket object
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    try:
        # Connect to a remote server (does not matter which)
        s.connect(("8.8.8.8", 80))
        # Get the IP address from the socket
        ip_address = s.getsockname()[0]
    except Exception as e:
        print("Error:", e)
        ip_address = "Unknown"
    finally:
        s.close()

    return ip_address

def scanAtemIP():
    global atemip
    for i in range(1,255):
        ip = f"192.168.212.{i}"
        switcher.ping(ip)
        
        if switcher.waitForConnection():
            print(f"[{time.ctime()}] ATEM switcher found at {ip}")
            with canvas(device) as draw:
                draw.text((5,20),f"atem ip:{ip}",fill="white")
            count = 255
            atemip = ip


###############################################################################

# start of mainline programm
            
###############################################################################


alert("mh-Startup.service")

if debuggingmode == False:
    time.sleep(2)

print(f"mhAPIv2 start at [{time.ctime()}]")
device.clear()
alert(f"atem service ...{time.time()}")

# connecting to devices
if ipknown == False:
    scanAtemIP()

# draw information to screen

device.clear()

with canvas(device) as draw:
    draw.text((10,10),"mhAIv2",fill="white")
    draw.text((10,20),f"rasp:{getRaspIP()}", fill="white")
    draw.text((10,30),f"atem:{atemip}",fill="white")

if debuggingmode == False:
    time.sleep(2)

# control fans 
pwmFanzone1 = GPIO.PWM(fanzone1Pin, 1000) 
pwmFanzone2 = GPIO.PWM(fanzone2Pin, 1000) 
pwmFanzone1.start(0) 
pwmFanzone2.start(0)



switcher.connect(atemip)
if switcher.waitForConnection(infinite=False):
    print("Atem connected")
    alert("atem connected")
else:
    print("Internal Error 1 - Atem not found")
    alert("Error Code 1")


alert("waiting for talbet...")

# general api calls
###############################################################################

@app.delete("/unitconnected")
def unitconnected():
    device.clear()
    bigstatus(": )")
    return {"page":"home"}


@app.put("/camera")
def cameraPower(power:bool):
    if power == True:
        GPIO.output(gpio3Pin,GPIO.LOW)
    else:
        GPIO.output(gpio3Pin,GPIO.HIGH)

@app.put("/relay")
def triggerRelay(power:bool):
    if power == True:
        GPIO.output(gpio4Pin,GPIO.LOW)
    else:
        GPIO.output(gpio4Pin,GPIO.HIGH)

# atem calls
###############################################################################

@app.put("/atemFTB")
def switchtoblack():
    switcher.execFadeToBlackME(0)
    global atemBlack
    atemBlack = not atemBlack
    return {"status":"success"}

@app.get("/atemGetFTB")
def ftb():
    return{"fadetoBlack":atemBlack}

@app.post("/atemProgram")
def switchchannel(channel:int):
    global atemProgramChannel
    atemProgramChannel = channel
    switcher.setProgramInputVideoSource(0,channel)
    return {"status":"success"}

@app.get("/atemGetProgram")
def getProgram():
    return {"program":atemProgramChannel}

@app.post("/atemPreview")
def switchchannel(channel:int):
    global atemPreviewChannel
    atemPreviewChannel = channel
    switcher.setPreviewInputVideoSource(0,channel)
    return {"status":"success"}

@app.get("/atemGetPreview")
def getPreview():
    return{"preview":atemPreviewChannel}


# table calls 
###############################################################################


# function nulls the table so the measuring starts at 70cm 
def nulltable():
    GPIO.output(motorDirPin,GPIO.LOW)
    motorPWM.ChangeDutyCycle(100)
    time.sleep(15)
    motorPWM.ChangeDutyCycle(0)
    global tableHeight
    tableHeight = tableStartHeight

def tableMoveUP(timesec:int,speed:int):
    GPIO.output(motorDirPin,GPIO.HIGH)
    motorPWM.ChangeDutyCycle(speed)
    time.sleep(timesec)
    motorPWM.ChangeDutyCycle(0)


def tableMoveDown(timesec:int,speed:int):
    GPIO.output(motorDirPin,GPIO.LOW)
    motorPWM.ChangeDutyCycle(speed)
    time.sleep(timesec)
    motorPWM.ChangeDutyCycle(0)

@app.put("/tableHeight")
def movetableHeight(height:int):
    # table moves up 3cm per second  
    global tableHeight 
    
    if height > tableHeight: #up
        diffHeight = height - tableHeight 
        driveDuration = (diffHeight)/ tableUpSpeedPerSec
        tableMoveUP(driveDuration,100)
        tableHeight = height
    if height < tableHeight: #down
        diffHeight = tableHeight - height
        driveDuration = (diffHeight)/ talbeDownSpeedPerSec
        tableMoveDown(driveDuration,100)
        tableHeight = height

    return {"current height":tableHeight}

@app.put("/tableUP")
def tableUP(timesec:int,speed:int):
    tableMoveUP(timesec,speed)


@app.put("/tableDown")
def tableDown(timesec:int,speed:int):
    tableMoveDown(timesec,speed)


@app.put("/tablemode")
def changetablemode(chmode:tableMode):
    global mode 
    mode = chmode
    return {"mode":mode}

@app.get("/tableCurrentHeight")
def getcurrentheight():
    return{"height":tableHeight}

@app.post("/tableNull")
def resetDesk():
    nulltable()

# light calls 
###############################################################################

@app.put("/lighton") # unused - to be replaced
def desklighton():
    GPIO.output(gpio6Pin,GPIO.LOW)
    time.sleep(1)
    GPIO.output(gpio6Pin, GPIO.HIGH)
    
# fan calls 
###############################################################################

@app.put("/fanautomode")
def automaticmode(on:bool):
    return {"fanlevel":"error"}

@app.put("/fanlevel")
def setfanspeed(fanlevel:int):
    pwmFanzone1.ChangeDutyCycle(fanlevel) 
    pwmFanzone2.ChangeDutyCycle(fanlevel)  
    return {"fanlevel":fanlevel}

###############################################################################

# hotkey functions
            
###############################################################################



@app.post("/configurehotkeys")
def configurenewHK(configuration: Configuration = Body(...)):
    global hotkey1Configuration
    global hotkey2Configuration
    global hotkey3Configuration

    if configuration.selectedTab == "meeting":
        hotkey1Configuration = configuration
    if configuration.selectedTab == "recording":
        hotkey2Configuration = configuration
    if configuration.selectedTab == "working":
        hotkey3Configuration = configuration
    print(configuration);
        


    return {"status":"saved"}

def hotkey1():
    global hotkey1Configuration
    movetableHeight(hotkey1Configuration.deskHeight)
    setfanspeed(hotkey1Configuration.fanSpeed)

    notify2("hotkey1 pressed")  

def hotkey2():
    
    global hotkey2Configuration
    movetableHeight(hotkey2Configuration.deskHeight)
    setfanspeed(hotkey2Configuration.fanSpeed)
    notify2("hotkey2 pressed")


def hotkey3():
    global hotkey3Configuration
    movetableHeight(hotkey3Configuration.deskHeight)
    setfanspeed(hotkey3Configuration.fanSpeed)
    notify2("hotkey3 pressed")

def coldkey1(channel):
    global hotkey1Configuration
    movetableHeight(hotkey1Configuration.deskHeight)
    setfanspeed(hotkey1Configuration.fanSpeed)

    notify2("key1 pressed")  

def coldkey2(channel):
    
    global hotkey2Configuration
    movetableHeight(hotkey2Configuration.deskHeight)
    setfanspeed(hotkey2Configuration.fanSpeed)
    notify2("key2 pressed")


def coldkey3(channel):
    global hotkey3Configuration
    movetableHeight(hotkey3Configuration.deskHeight)
    setfanspeed(hotkey3Configuration.fanSpeed)
    notify2("key3 pressed")


@app.post("/hotkey")
def handlehot(key):
    print(key)
    if key == "1":
        hotkey1()
        alert("hotkey1 triggered")
        return {"status":"hotkey1 triggered"}
    if key == "2":
        hotkey2()
        alert("hotkey2 triggered")
        return {"status":"hotkey2 triggered"}
    if key == "3":
        hotkey3()
        alert("hotkey3 triggered")
        return {"status":"hotkey3 triggered"}


# button events 
GPIO.add_event_detect(button1Pin, GPIO.FALLING, callback=coldkey1, bouncetime=200)
GPIO.add_event_detect(button2Pin, GPIO.FALLING, callback=coldkey2, bouncetime=200)
GPIO.add_event_detect(button3Pin, GPIO.FALLING, callback=coldkey3, bouncetime=200)