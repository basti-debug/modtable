# meetinghub
meetinghub my final thesis at the HTL Rankweil.

This thesis tries to simplifiy the workinglife of an office worker which needs the desk mainly for connecting to meetings, creating small explanatory videos and do basic office work. 

For this the old setup contained a hefty amount of cables, controllers and other distracting things. To counteract these parts a minimalistic table was designed and developed. 


# Document
To get a deep overview about the project, please read my final thesis [here](documents/DiplomarbeitMeetinghubV4.pdf).

## Overview file structure
### API
The folder [`/api`](api) houses all the needed APIs for the project, so the meetinghubAPI v2 and the older versions, also the test versions can be found under `/api/switch api test`. The .dll is the c# API provided by Blackmagic, for more information visit: [Blackmagic Developer Website](https://www.blackmagicdesign.com/developer/). 

The newest version of the API uses the [PyAtemMax Library](https://clvlabs.github.io/PyATEMMax/) and is a python [FastAPI](https://fastapi.tiangolo.com/). 

### App
The [`/app`](app) folder houses the first attempts on creating a [flutter](https://flutter.dev/) app, this subproject was canceled and replaced by the website, which can be found in [`/website`](website).

### Documentation
The documentation can be found in `/documents`, where the diploma thesis documentation is situated. It is written in LaTex, the published version can be found at `/documents/v2` and the pdf version [here](documents/DiplomarbeitMeetinghubV4.pdf).

The big revisions v1 and v2 are due to changed regulations at our school. 

### Firmware
The `/firmware` folder contains the firmware for the v1 with the esp32, in the folder `/firmware/esp32/meetinghubfirmwarev1`, and the second version for the raspberry pi under `firmware/raspberry`.

### Models 
All the needed 3d models to assemble the desk it self can be found in the folder `/models`. With `/models/mounts` housing all the 3d printed parts, and `/models/table model` housing the 3d models of the construction of the desk itself. 

Instructions on how th assemble the 3d printed parts can be found [here](models/mounts/Assembly%20instructions%20Lamp.pdf).

### PCBs
The needed pcbs for the first prototype, including the hbridge etc, can be found at [`/pcb/meetinghubPCBv1`](pcb/meetinghubPCBv1/). 
The raspberry pi shield can be found at [`/pcb/meetinghubPCBv2`](pcb/meetinghubPCBv2/)

### Website
The above mentioned website is a [react.js](https://react.dev/) website which can be hosted on the raspberry pi via ngnix.

