\contentsline {section}{\numberline {1}Abbreviations}{1}{section.1}%
\contentsline {section}{\numberline {2}Affidavit}{2}{section.2}%
\contentsline {section}{\numberline {3}Thanks to}{3}{section.3}%
\contentsline {section}{\numberline {4}Document information}{4}{section.4}%
\contentsline {part}{I\hspace {1em}Prelude}{5}{part.1}%
\contentsline {section}{\numberline {5}Abstract - English}{5}{section.5}%
\contentsline {subsection}{\numberline {5.1}Initial solution}{5}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Aim of the project}{5}{subsection.5.2}%
\contentsline {section}{\numberline {6}Abstract - German}{6}{section.6}%
\contentsline {subsection}{\numberline {6.1}Ausgangssituation}{6}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Ziel des Projekts}{6}{subsection.6.2}%
\contentsline {section}{\numberline {7}Product Requirements Document}{7}{section.7}%
\contentsline {subsection}{\numberline {7.1}Introduction}{7}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Initial situation}{7}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Target situation}{7}{subsection.7.3}%
\contentsline {subsection}{\numberline {7.4}Use cases}{7}{subsection.7.4}%
\contentsline {subsection}{\numberline {7.5}Interfaces}{8}{subsection.7.5}%
\contentsline {subsection}{\numberline {7.6}System requirements}{8}{subsection.7.6}%
\contentsline {subsection}{\numberline {7.7}Quality requirements}{8}{subsection.7.7}%
\contentsline {subsection}{\numberline {7.8}Risks}{8}{subsection.7.8}%
\contentsline {section}{\numberline {8}Project Team}{9}{section.8}%
\contentsline {subsection}{\numberline {8.1}Project members}{9}{subsection.8.1}%
\contentsline {subsection}{\numberline {8.2}Supervising teacher}{9}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Contact person}{9}{subsection.8.3}%
\contentsline {section}{\numberline {9}Project planning}{10}{section.9}%
\contentsline {subsection}{\numberline {9.1}Milestone 1 - API}{10}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Milestone 2 - Website}{11}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}Milestone 3 - Monitor Stand}{11}{subsection.9.3}%
\contentsline {subsection}{\numberline {9.4}Milestone 4 - Controller boards}{11}{subsection.9.4}%
\contentsline {subsection}{\numberline {9.5}Milestone 5 - Table Top}{11}{subsection.9.5}%
\contentsline {subsection}{\numberline {9.6}Milestone 6 - Lights}{11}{subsection.9.6}%
\contentsline {subsection}{\numberline {9.7}Milestone 6.1 - Light bar}{11}{subsection.9.7}%
\contentsline {subsection}{\numberline {9.8}Milestone 7 - Client}{11}{subsection.9.8}%
\contentsline {section}{\numberline {10}Timeline}{12}{section.10}%
\contentsline {section}{\numberline {11}Time spent}{13}{section.11}%
\contentsline {section}{\numberline {12}Goals}{14}{section.12}%
\contentsline {subsection}{\numberline {12.1}Must have}{14}{subsection.12.1}%
\contentsline {subsection}{\numberline {12.2}Nice to have}{14}{subsection.12.2}%
\contentsline {subsection}{\numberline {12.3}Not have}{14}{subsection.12.3}%
\contentsline {part}{II\hspace {1em}Requirements}{15}{part.2}%
\contentsline {section}{\numberline {13}Used software}{15}{section.13}%
\contentsline {subsection}{\numberline {13.1}General Software}{15}{subsection.13.1}%
\contentsline {subsubsection}{\numberline {13.1.1}GitLab}{15}{subsubsection.13.1.1}%
\contentsline {paragraph}{Why GitLab}{16}{subsubsection.13.1.1}%
\contentsline {subsubsection}{\numberline {13.1.2}TeXStudio}{17}{subsubsection.13.1.2}%
\contentsline {paragraph}{todonotes package}{17}{subsubsection.13.1.2}%
\contentsline {paragraph}{Zotero\blx@tocontentsinit {0}\autocite {zotero}}{17}{figure.caption.7}%
\contentsline {paragraph}{tcolorbox}{18}{figure.caption.7}%
\contentsline {subsection}{\numberline {13.2}Construction software}{18}{subsection.13.2}%
\contentsline {subsubsection}{\numberline {13.2.1}Solidworks}{18}{subsubsection.13.2.1}%
\contentsline {subsubsection}{\numberline {13.2.2}Altium}{18}{subsubsection.13.2.2}%
\contentsline {subsection}{\numberline {13.3}Coding Software}{19}{subsection.13.3}%
\contentsline {subsubsection}{\numberline {13.3.1}Visual Studio Code}{19}{subsubsection.13.3.1}%
\contentsline {paragraph}{Remote Debugging Extension}{19}{figure.caption.9}%
\contentsline {subsubsection}{\numberline {13.3.2}Visual Studio}{19}{subsubsection.13.3.2}%
\contentsline {subsubsection}{\numberline {13.3.3}Termius}{19}{subsubsection.13.3.3}%
\contentsline {subsection}{\numberline {13.4}Time tracking}{20}{subsection.13.4}%
\contentsline {subsubsection}{\numberline {13.4.1}Gitlab}{20}{subsubsection.13.4.1}%
\contentsline {subsubsection}{\numberline {13.4.2}Clockify}{20}{subsubsection.13.4.2}%
\contentsline {section}{\numberline {14}Required basics}{21}{section.14}%
\contentsline {subsection}{\numberline {14.1}H-Bridge}{21}{subsection.14.1}%
\contentsline {subsection}{\numberline {14.2}I2C}{22}{subsection.14.2}%
\contentsline {subsection}{\numberline {14.3}API}{22}{subsection.14.3}%
\contentsline {part}{III\hspace {1em}Developing Concept}{23}{part.3}%
\contentsline {section}{\numberline {15}Functional overview}{23}{section.15}%
\contentsline {subsection}{\numberline {15.1}Controlling the height of the desk}{23}{subsection.15.1}%
\contentsline {subsection}{\numberline {15.2}Controlling the camera}{23}{subsection.15.2}%
\contentsline {subsection}{\numberline {15.3}Lighting conditions}{24}{subsection.15.3}%
\contentsline {subsection}{\numberline {15.4}Monitor Stand}{24}{subsection.15.4}%
\contentsline {subsection}{\numberline {15.5}Connect to the desk easily}{24}{subsection.15.5}%
\contentsline {section}{\numberline {16}Operations concept}{25}{section.16}%
\contentsline {subsection}{\numberline {16.1}Hotkeys}{25}{subsection.16.1}%
\contentsline {section}{\numberline {17}Software concept}{26}{section.17}%
\contentsline {subsection}{\numberline {17.1}Development controller}{26}{subsection.17.1}%
\contentsline {subsection}{\numberline {17.2}Deployed controller}{26}{subsection.17.2}%
\contentsline {subsection}{\numberline {17.3}Tablet-Website}{26}{subsection.17.3}%
\contentsline {subsection}{\numberline {17.4}Windows Client}{27}{subsection.17.4}%
\contentsline {part}{IV\hspace {1em}Technical Documentation}{28}{part.4}%
\contentsline {section}{\numberline {18}Technical overview}{28}{section.18}%
\contentsline {subsection}{\numberline {18.1}Right Compartment}{30}{subsection.18.1}%
\contentsline {subsubsection}{\numberline {18.1.1}Fans}{30}{subsubsection.18.1.1}%
\contentsline {paragraph}{80mm Fans}{30}{figure.caption.20}%
\contentsline {paragraph}{40mm Fans}{30}{figure.caption.20}%
\contentsline {paragraph}{Air in the Right compartment}{31}{figure.caption.21}%
\contentsline {paragraph}{Air in the Left compartment}{31}{figure.caption.21}%
\contentsline {subparagraph}{How effective is the cooling?}{32}{figure.caption.21}%
\contentsline {subsubsection}{\numberline {18.1.2}Blackmagic Design ATEM Mini Pro}{33}{subsubsection.18.1.2}%
\contentsline {paragraph}{ATEM Software Control}{33}{figure.caption.24}%
\contentsline {paragraph}{Needed disassembly}{33}{figure.caption.24}%
\contentsline {subsubsection}{\numberline {18.1.3}Raspberry Pi}{35}{subsubsection.18.1.3}%
\contentsline {subsubsection}{\numberline {18.1.4}Cytron MD10C}{36}{subsubsection.18.1.4}%
\contentsline {subsubsection}{\numberline {18.1.5}Power Supplies}{37}{subsubsection.18.1.5}%
\contentsline {paragraph}{5V Power Supply}{37}{subsubsection.18.1.5}%
\contentsline {paragraph}{12V Power Supply}{38}{table.caption.30}%
\contentsline {subsection}{\numberline {18.2}Left Compartment}{39}{subsection.18.2}%
\contentsline {subsubsection}{\numberline {18.2.1}Thunderbolt Dock}{39}{subsubsection.18.2.1}%
\contentsline {paragraph}{Why this dock}{40}{subsubsection.18.2.1}%
\contentsline {subsubsection}{\numberline {18.2.2}Power supplies}{40}{subsubsection.18.2.2}%
\contentsline {paragraph}{Dock power supply}{40}{subsubsection.18.2.2}%
\contentsline {paragraph}{Motor power supply}{40}{subsubsection.18.2.2}%
\contentsline {subsection}{\numberline {18.3}Monitor Stand}{41}{subsection.18.3}%
\contentsline {subsubsection}{\numberline {18.3.1}Camera}{42}{subsubsection.18.3.1}%
\contentsline {subsubsection}{\numberline {18.3.2}Microphone}{42}{subsubsection.18.3.2}%
\contentsline {subsubsection}{\numberline {18.3.3}Monitor}{43}{subsubsection.18.3.3}%
\contentsline {subsubsection}{\numberline {18.3.4}Lights}{44}{subsubsection.18.3.4}%
\contentsline {section}{\numberline {19}Tabletop}{47}{section.19}%
\contentsline {section}{\numberline {20}Compartments}{50}{section.20}%
\contentsline {section}{\numberline {21}Central area of the desk}{52}{section.21}%
\contentsline {subsection}{\numberline {21.1}Ethernet Switch}{52}{subsection.21.1}%
\contentsline {section}{\numberline {22}operating terminal}{53}{section.22}%
\contentsline {subsection}{\numberline {22.1}Hotkeys}{53}{subsection.22.1}%
\contentsline {subsection}{\numberline {22.2}Status Screen}{53}{subsection.22.2}%
\contentsline {section}{\numberline {23}Table motor}{54}{section.23}%
\contentsline {section}{\numberline {24}Used microcontrollers}{56}{section.24}%
\contentsline {subsection}{\numberline {24.1}Topology}{56}{subsection.24.1}%
\contentsline {subsubsection}{\numberline {24.1.1}v1 - Development version}{56}{subsubsection.24.1.1}%
\contentsline {subsubsection}{\numberline {24.1.2}v2 - Deployed version}{57}{subsubsection.24.1.2}%
\contentsline {subsection}{\numberline {24.2}Esp32}{57}{subsection.24.2}%
\contentsline {subsubsection}{\numberline {24.2.1}Information about the Microcontroller}{57}{subsubsection.24.2.1}%
\contentsline {subsubsection}{\numberline {24.2.2}Implementation}{58}{subsubsection.24.2.2}%
\contentsline {subsubsection}{\numberline {24.2.3}Integrated development environment}{58}{subsubsection.24.2.3}%
\contentsline {subsection}{\numberline {24.3}Raspberry Pi}{58}{subsection.24.3}%
\contentsline {section}{\numberline {25}Adapter PCBs}{59}{section.25}%
\contentsline {subsection}{\numberline {25.1}prototype 1}{59}{subsection.25.1}%
\contentsline {subsubsection}{\numberline {25.1.1}Status Screen}{59}{subsubsection.25.1.1}%
\contentsline {subsubsection}{\numberline {25.1.2}H-Bridge}{60}{subsubsection.25.1.2}%
\contentsline {paragraph}{Used H-Bridge Chip - ROHM BD62130}{60}{subsubsection.25.1.2}%
\contentsline {subparagraph}{How the IC was driven}{61}{figure.caption.60}%
\contentsline {paragraph}{Possible errors}{61}{table.caption.61}%
\contentsline {subparagraph}{Theory 1 - wrong command}{61}{table.caption.61}%
\contentsline {subparagraph}{Theory 2 - no external over current diodes}{61}{table.caption.61}%
\contentsline {subparagraph}{Theory 3 - over current at motor start}{62}{figure.caption.62}%
\contentsline {paragraph}{Conclusion}{63}{figure.caption.65}%
\contentsline {subsubsection}{\numberline {25.1.3}Fans}{63}{subsubsection.25.1.3}%
\contentsline {subsubsection}{\numberline {25.1.4}Servo}{65}{subsubsection.25.1.4}%
\contentsline {subsubsection}{\numberline {25.1.5}Buttons}{65}{subsubsection.25.1.5}%
\contentsline {subsubsection}{\numberline {25.1.6}Power}{65}{subsubsection.25.1.6}%
\contentsline {subsubsection}{\numberline {25.1.7}Additional Pins}{65}{subsubsection.25.1.7}%
\contentsline {subsection}{\numberline {25.2}prototype 2}{66}{subsection.25.2}%
\contentsline {subsubsection}{\numberline {25.2.1}Motor control-board}{66}{subsubsection.25.2.1}%
\contentsline {subsubsection}{\numberline {25.2.2}Fans}{67}{subsubsection.25.2.2}%
\contentsline {subsubsection}{\numberline {25.2.3}OLED screen}{67}{subsubsection.25.2.3}%
\contentsline {subsubsection}{\numberline {25.2.4}Light section}{68}{subsubsection.25.2.4}%
\contentsline {subsubsection}{\numberline {25.2.5}Buttons}{68}{subsubsection.25.2.5}%
\contentsline {subsubsection}{\numberline {25.2.6}Additional Pins}{68}{subsubsection.25.2.6}%
\contentsline {section}{\numberline {26}Firmware}{69}{section.26}%
\contentsline {subsection}{\numberline {26.1}Firmware version 1}{69}{subsection.26.1}%
\contentsline {subsubsection}{\numberline {26.1.1}Handling incoming I2C}{70}{subsubsection.26.1.1}%
\contentsline {subsubsection}{\numberline {26.1.2}Fans}{71}{subsubsection.26.1.2}%
\contentsline {subsubsection}{\numberline {26.1.3}H-Bridge}{72}{subsubsection.26.1.3}%
\contentsline {subsection}{\numberline {26.2}Firmware version 2}{73}{subsection.26.2}%
\contentsline {section}{\numberline {27}API}{74}{section.27}%
\contentsline {subsection}{\numberline {27.1}API version 1 - Blackmagic Switchers API}{74}{subsection.27.1}%
\contentsline {paragraph}{The DLL is a Win32 DLL}{76}{figure.caption.85}%
\contentsline {subsection}{\numberline {27.2}API version 2 - FastAPI}{77}{subsection.27.2}%
\contentsline {subsubsection}{\numberline {27.2.1}PyATEMMax}{77}{subsubsection.27.2.1}%
\contentsline {subsubsection}{\numberline {27.2.2}FastAPI}{78}{subsubsection.27.2.2}%
\contentsline {subsubsection}{\numberline {27.2.3}Table calls}{78}{subsubsection.27.2.3}%
\contentsline {subsection}{\numberline {27.3}Hotkeys}{79}{subsection.27.3}%
\contentsline {section}{\numberline {28}Website}{81}{section.28}%
\contentsline {subsection}{\numberline {28.1}ReactJS}{81}{subsection.28.1}%
\contentsline {subsection}{\numberline {28.2}Basic structure of the website}{82}{subsection.28.2}%
\contentsline {subsubsection}{\numberline {28.2.1}Overview}{83}{subsubsection.28.2.1}%
\contentsline {paragraph}{Home Page}{83}{subsubsection.28.2.1}%
\contentsline {paragraph}{Calendar Page}{84}{figure.caption.94}%
\contentsline {paragraph}{Presets Page}{85}{figure.caption.95}%
\contentsline {paragraph}{Settings Page}{87}{figure.caption.97}%
\contentsline {subsubsection}{\numberline {28.2.2}Important Components}{88}{subsubsection.28.2.2}%
\contentsline {paragraph}{Router}{88}{subsubsection.28.2.2}%
\contentsline {paragraph}{NextUI Component - Tabs}{89}{figure.caption.99}%
\contentsline {section}{\numberline {29}Windows Client}{90}{section.29}%
\contentsline {part}{V\hspace {1em}Conclusion}{91}{part.5}%
\contentsline {section}{\numberline {30}Did the implementation work as planned?}{91}{section.30}%
\contentsline {section}{\numberline {31}Occurred errors}{91}{section.31}%
\contentsline {subsection}{\numberline {31.1}H-Bridge problem}{91}{subsection.31.1}%
\contentsline {subsection}{\numberline {31.2}ASP NET errors}{91}{subsection.31.2}%
\contentsline {part}{VI\hspace {1em}directories}{91}{part.6}%
\contentsline {section}{\numberline {32}Attachments}{104}{section.32}%
\contentsline {subsection}{\numberline {32.1}All API calls}{104}{subsection.32.1}%
\contentsline {subsection}{\numberline {32.2}\ignorespaces Project manual}{105}{subsection.32.2}%
\contentsline {subsection}{\numberline {32.3}Schematic v1}{120}{subsection.32.3}%
\contentsline {subsection}{\numberline {32.4}PCB v1}{121}{subsection.32.4}%
\contentsline {subsection}{\numberline {32.5}Schematic v2}{122}{subsection.32.5}%
\contentsline {subsection}{\numberline {32.6}PCB v2}{123}{subsection.32.6}%
\contentsline {subsection}{\numberline {32.7}PCB v2 Bill of Material}{124}{subsection.32.7}%
\contentsline {subsection}{\numberline {32.8}Drawing of stand}{125}{subsection.32.8}%
\contentsline {subsection}{\numberline {32.9}Drawing of tabletop}{126}{subsection.32.9}%
\contentsline {subsection}{\numberline {32.10}Time sheet}{127}{subsection.32.10}%
