\contentsline {section}{\numberline {1}Abbreviations}{10}{section.1}%
\contentsline {section}{\numberline {2}Affidavit}{11}{section.2}%
\contentsline {section}{\numberline {3}Acknowledgements}{12}{section.3}%
\contentsline {part}{I\hspace {1em}Prelude}{13}{part.1}%
\contentsline {section}{\numberline {4}Abstract - English}{13}{section.4}%
\contentsline {subsection}{\numberline {4.1}Initial solution}{13}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Aim of the project}{13}{subsection.4.2}%
\contentsline {section}{\numberline {5}Abstract - German}{14}{section.5}%
\contentsline {subsection}{\numberline {5.1}Ausgangssituation}{14}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Ziel des Projekts}{14}{subsection.5.2}%
\contentsline {section}{\numberline {6}Used software}{15}{section.6}%
\contentsline {subsection}{\numberline {6.1}GitLab}{15}{subsection.6.1}%
\contentsline {paragraph}{Why GitLab}{15}{section*.5}%
\contentsline {subsection}{\numberline {6.2}TeXStudio}{16}{subsection.6.2}%
\contentsline {subsubsection}{\numberline {6.2.1}todonotes package}{17}{subsubsection.6.2.1}%
\contentsline {subsubsection}{\numberline {6.2.2}Zotero}{17}{subsubsection.6.2.2}%
\contentsline {subsubsection}{\numberline {6.2.3}tcolorbox}{17}{subsubsection.6.2.3}%
\contentsline {section}{\numberline {7}Construction software}{18}{section.7}%
\contentsline {subsection}{\numberline {7.1}Solidworks}{18}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Altium}{18}{subsection.7.2}%
\contentsline {section}{\numberline {8}Coding Software}{18}{section.8}%
\contentsline {subsection}{\numberline {8.1}Visual Studio Code}{18}{subsection.8.1}%
\contentsline {subsubsection}{\numberline {8.1.1}Remote Debugging Extension}{18}{subsubsection.8.1.1}%
\contentsline {subsection}{\numberline {8.2}Visual Studio}{18}{subsection.8.2}%
\contentsline {subsection}{\numberline {8.3}Termius}{19}{subsection.8.3}%
\contentsline {section}{\numberline {9}Time tracking}{19}{section.9}%
\contentsline {subsubsection}{\numberline {9.0.1}Gitlab}{19}{subsubsection.9.0.1}%
\contentsline {subsubsection}{\numberline {9.0.2}Clockify}{19}{subsubsection.9.0.2}%
\contentsline {part}{II\hspace {1em}Requirements}{20}{part.2}%
\contentsline {section}{\numberline {10}Product Requirements Document}{20}{section.10}%
\contentsline {subsection}{\numberline {10.1}Introduction}{20}{subsection.10.1}%
\contentsline {subsection}{\numberline {10.2}Initial situation}{20}{subsection.10.2}%
\contentsline {subsection}{\numberline {10.3}Target situation}{20}{subsection.10.3}%
\contentsline {subsection}{\numberline {10.4}Use cases}{20}{subsection.10.4}%
\contentsline {subsection}{\numberline {10.5}Interfaces}{21}{subsection.10.5}%
\contentsline {subsection}{\numberline {10.6}System requirements}{21}{subsection.10.6}%
\contentsline {subsection}{\numberline {10.7}Quality requirements}{21}{subsection.10.7}%
\contentsline {subsection}{\numberline {10.8}Risks}{21}{subsection.10.8}%
\contentsline {section}{\numberline {11}Required basics}{22}{section.11}%
\contentsline {subsection}{\numberline {11.1}H-Bridge}{22}{subsection.11.1}%
\contentsline {subsection}{\numberline {11.2}I2C}{22}{subsection.11.2}%
\contentsline {subsection}{\numberline {11.3}API}{22}{subsection.11.3}%
\contentsline {part}{III\hspace {1em}Developing Concept}{23}{part.3}%
\contentsline {section}{\numberline {12}Project planning}{23}{section.12}%
\contentsline {section}{\numberline {13}SMART Analyse}{23}{section.13}%
\contentsline {section}{\numberline {14}Goals}{24}{section.14}%
\contentsline {subsection}{\numberline {14.1}Must have}{24}{subsection.14.1}%
\contentsline {subsection}{\numberline {14.2}Nice to have}{24}{subsection.14.2}%
\contentsline {subsection}{\numberline {14.3}Not have}{24}{subsection.14.3}%
\contentsline {section}{\numberline {15}Project Team}{24}{section.15}%
\contentsline {subsection}{\numberline {15.1}Supervising teacher}{24}{subsection.15.1}%
\contentsline {subsection}{\numberline {15.2}Project members}{24}{subsection.15.2}%
\contentsline {section}{\numberline {16}Functional overview}{25}{section.16}%
\contentsline {subsection}{\numberline {16.1}Control interface}{25}{subsection.16.1}%
\contentsline {subsection}{\numberline {16.2}Height of the table}{25}{subsection.16.2}%
\contentsline {subsection}{\numberline {16.3}Camera control}{25}{subsection.16.3}%
\contentsline {subsubsection}{\numberline {16.3.1}Microphone control}{25}{subsubsection.16.3.1}%
\contentsline {subsection}{\numberline {16.4}Lighting situation}{25}{subsection.16.4}%
\contentsline {section}{\numberline {17}Operations concept}{25}{section.17}%
\contentsline {subsection}{\numberline {17.1}Hotkeys}{25}{subsection.17.1}%
\contentsline {subsection}{\numberline {17.2}Tablet}{26}{subsection.17.2}%
\contentsline {subsection}{\numberline {17.3}Client}{26}{subsection.17.3}%
\contentsline {section}{\numberline {18}Software concept}{27}{section.18}%
\contentsline {subsection}{\numberline {18.1}Firmware concept}{27}{subsection.18.1}%
\contentsline {subsection}{\numberline {18.2}Client concept}{27}{subsection.18.2}%
\contentsline {part}{IV\hspace {1em}Technical Documentation}{28}{part.4}%
\contentsline {section}{\numberline {19}Technical overview}{28}{section.19}%
\contentsline {subsection}{\numberline {19.1}Right Compartment}{30}{subsection.19.1}%
\contentsline {subsubsection}{\numberline {19.1.1}Fans}{30}{subsubsection.19.1.1}%
\contentsline {paragraph}{80mm Fans}{30}{section*.31}%
\contentsline {paragraph}{40mm Fans}{30}{section*.32}%
\contentsline {paragraph}{Air in the Right compartment}{31}{section*.35}%
\contentsline {paragraph}{Air in the Left compartment}{31}{section*.36}%
\contentsline {subsubsection}{\numberline {19.1.2}Blackmagic Design ATEM Mini Pro}{32}{subsubsection.19.1.2}%
\contentsline {paragraph}{ATEM Software Control}{32}{section*.39}%
\contentsline {paragraph}{Needed disassembly}{32}{section*.40}%
\contentsline {subsubsection}{\numberline {19.1.3}Raspberry Pi}{34}{subsubsection.19.1.3}%
\contentsline {subsubsection}{\numberline {19.1.4}Cytron MD10C}{34}{subsubsection.19.1.4}%
\contentsline {subsubsection}{\numberline {19.1.5}Power Supplies}{35}{subsubsection.19.1.5}%
\contentsline {paragraph}{5V Power Supply}{35}{figure.caption.47}%
\contentsline {paragraph}{12V Power Supply}{36}{section*.50}%
\contentsline {subsection}{\numberline {19.2}Left Compartment}{37}{subsection.19.2}%
\contentsline {subsubsection}{\numberline {19.2.1}Thunderbolt Dock}{37}{subsubsection.19.2.1}%
\contentsline {paragraph}{Why this dock}{38}{section*.55}%
\contentsline {subsubsection}{\numberline {19.2.2}Power supplies}{38}{subsubsection.19.2.2}%
\contentsline {paragraph}{Dock power supply}{38}{section*.56}%
\contentsline {paragraph}{Motor power supply}{38}{section*.58}%
\contentsline {subsection}{\numberline {19.3}Monitor Stand}{38}{subsection.19.3}%
\contentsline {subsubsection}{\numberline {19.3.1}Camera}{38}{subsubsection.19.3.1}%
\contentsline {subsubsection}{\numberline {19.3.2}Microphone}{38}{subsubsection.19.3.2}%
\contentsline {subsubsection}{\numberline {19.3.3}Monitor}{38}{subsubsection.19.3.3}%
\contentsline {subsubsection}{\numberline {19.3.4}Lights}{38}{subsubsection.19.3.4}%
\contentsline {section}{\numberline {20}Base Table}{39}{section.20}%
\contentsline {section}{\numberline {21}Compartments}{41}{section.21}%
\contentsline {section}{\numberline {22}Central area of the desk}{43}{section.22}%
\contentsline {subsection}{\numberline {22.1}Ethernet Switch}{43}{subsection.22.1}%
\contentsline {section}{\numberline {23}operating terminal}{44}{section.23}%
\contentsline {subsection}{\numberline {23.1}Hotkeys}{44}{subsection.23.1}%
\contentsline {subsection}{\numberline {23.2}Status Screen}{44}{subsection.23.2}%
\contentsline {section}{\numberline {24}Microcontrollers}{45}{section.24}%
\contentsline {subsection}{\numberline {24.1}Topology}{45}{subsection.24.1}%
\contentsline {subsubsection}{\numberline {24.1.1}v1 - Development version}{45}{subsubsection.24.1.1}%
\contentsline {subsubsection}{\numberline {24.1.2}v2 - Deployed version}{45}{subsubsection.24.1.2}%
\contentsline {subsection}{\numberline {24.2}Esp32}{46}{subsection.24.2}%
\contentsline {subsubsection}{\numberline {24.2.1}Information about the Processor}{46}{subsubsection.24.2.1}%
\contentsline {subsubsection}{\numberline {24.2.2}Implementation}{46}{subsubsection.24.2.2}%
\contentsline {subsubsection}{\numberline {24.2.3}Software}{47}{subsubsection.24.2.3}%
\contentsline {subsection}{\numberline {24.3}Raspberry Pi}{47}{subsection.24.3}%
\contentsline {section}{\numberline {25}PCB}{48}{section.25}%
\contentsline {subsection}{\numberline {25.1}prototype 1}{48}{subsection.25.1}%
\contentsline {subsubsection}{\numberline {25.1.1}Status Screen}{48}{subsubsection.25.1.1}%
\contentsline {subsubsection}{\numberline {25.1.2}H-Bridge}{48}{subsubsection.25.1.2}%
\contentsline {paragraph}{Used H-Bridge Chip - ROHM BD62130}{48}{section*.77}%
\contentsline {subparagraph}{How the IC was driven}{49}{section*.80}%
\contentsline {paragraph}{Possible errors}{50}{section*.82}%
\contentsline {subparagraph}{Theory 1 - wrong command}{50}{section*.83}%
\contentsline {subparagraph}{Theory 2 - no external over current diodes}{51}{section*.85}%
\contentsline {subparagraph}{Theory 3 - over current at motor start}{51}{section*.87}%
\contentsline {paragraph}{Conclusion}{52}{section*.91}%
\contentsline {subsubsection}{\numberline {25.1.3}Fans}{52}{subsubsection.25.1.3}%
\contentsline {subsubsection}{\numberline {25.1.4}Servo}{53}{subsubsection.25.1.4}%
\contentsline {subsubsection}{\numberline {25.1.5}Buttons}{54}{subsubsection.25.1.5}%
\contentsline {subsubsection}{\numberline {25.1.6}Power}{54}{subsubsection.25.1.6}%
\contentsline {subsubsection}{\numberline {25.1.7}Additional Pins}{54}{subsubsection.25.1.7}%
\contentsline {subsection}{\numberline {25.2}prototype 2}{55}{subsection.25.2}%
\contentsline {subsubsection}{\numberline {25.2.1}Motor control-board}{55}{subsubsection.25.2.1}%
\contentsline {subsubsection}{\numberline {25.2.2}Fans}{55}{subsubsection.25.2.2}%
\contentsline {subsubsection}{\numberline {25.2.3}OLED screen}{56}{subsubsection.25.2.3}%
\contentsline {subsubsection}{\numberline {25.2.4}Light section}{57}{subsubsection.25.2.4}%
\contentsline {subsubsection}{\numberline {25.2.5}Buttons}{57}{subsubsection.25.2.5}%
\contentsline {subsubsection}{\numberline {25.2.6}Additional Pins}{57}{subsubsection.25.2.6}%
\contentsline {section}{\numberline {26}firmware}{59}{section.26}%
\contentsline {subsection}{\numberline {26.1}Firmware for the Esp 32}{59}{subsection.26.1}%
\contentsline {subsection}{\numberline {26.2}Firmware for the Raspberry Pi 3B+}{59}{subsection.26.2}%
\contentsline {section}{\numberline {27}Website}{59}{section.27}%
\contentsline {section}{\numberline {28}Windows Client}{59}{section.28}%
\contentsline {section}{\numberline {29}API}{59}{section.29}%
\contentsline {subsection}{\numberline {29.1}Blackmagic Switchers API}{59}{subsection.29.1}%
\contentsline {subsection}{\numberline {29.2}meetinghubAPI}{59}{subsection.29.2}%
\contentsline {section}{\numberline {30}Lighting}{59}{section.30}%
\contentsline {part}{V\hspace {1em}Conclusion}{59}{part.5}%
\contentsline {section}{\numberline {31}TeX Studio}{59}{section.31}%
\contentsline {section}{\numberline {32}HBridge}{59}{section.32}%
\contentsline {section}{\numberline {33}XAMPP}{60}{section.33}%
\contentsline {section}{\numberline {34}ASP NET ERRORS}{60}{section.34}%
\contentsline {part}{VI\hspace {1em}directories}{60}{part.6}%
